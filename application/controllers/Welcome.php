<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Welcome extends CI_Controller {
    public function __construct() {
        parent:: __construct();
        $this->load->model('model');
    }

	public function index() {
        $data['result'] = $this->model->getAllData();
		$this->load->view('welcome_message', $data);
    }

    public function create() {
        $this->model->createData();
        redirect("Welcome");
    }

    public function edit($id) {
        $data['row'] = $this->model->getData($id);
        $this->load->view('edit', $data);
    }

    public function update($id) {
        $this->model->updateData($id);
        redirect("Welcome");
    }

    public function delete($id) {
        $this->model->deleteData($id);
        redirect("Welcome");
    }
}
