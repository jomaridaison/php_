  
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Edit</title>
<!--     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
      <div class="container">
    <br>
    <br>
    <form method="post" action="<?php echo site_url('/Welcome/update')?>/<?php echo $row->id;?>">
                    <div class="form-group">
                        <label for="Fname">First Name</label>
                        <input type="text" class="form-control" name="FName"  value="<?php echo $row->FirstName; ?>" aria-describedby="emailHelp" placeholder="Enter First name">
                    </div>
                    <div class="form-group">
                        <label for="LName">First Name</label>
                        <input type="text" class="form-control" name="LName"  value="<?php echo $row->LastName; ?>" aria-describedby="emailHelp" placeholder="Enter Last name">
                    </div>
                    <div class="form-group">
                        <label for="DateReg">Date Register</label>
                        <input type="date" class="form-control" name="DateRegister"  value="<?php echo $row->DateRegister; ?>" aria-describedby="emailHelp" placeholder="Enter date">
                    </div>
                    <div class="form-group">
                        <label for="Address">Address</label>
                        <input type="text" class="form-control" name="Address"  value="<?php echo $row->Address; ?>" aria-describedby="emailHelp" placeholder="Enter Address">
                    </div>
                    <div class="form-group">
                        <label for="Phone">Phone</label>
                         <input class="form-control" type="tel" value="09061002834" name="Phone"  value="<?php echo $row->Phone; ?>" placeholder="Enter Contact number">
                    </div>
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="text" class="form-control" name="Email"  value="<?php echo $row->Email; ?>" aria-describedby="emailHelp" placeholder="Enter Email">
                    </div>
                    <button type="submit" class="btn btn-primary" value="save">Submit</button>
                </form> 
    </div>
</body>
</html>
