<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Dashboard</title>
<!--     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
        table tr td:last-child{
            width: 120px;
        }
    </style>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="mt-5 mb-3 clearfix">
                        <h2 class="pull-left">Demo exam</h2>
                        <div class="input-group">
                         


                        </div>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> Register </button>
                    </div>

                    <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo site_url('/Welcome/create')?>">
                    <div class="form-group">
                        <label for="Fname">First Name</label>
                        <input type="text" class="form-control" name="FName" aria-describedby="emailHelp" placeholder="Enter First name">
                    </div>
                    <div class="form-group">
                        <label for="LName">First Name</label>
                        <input type="text" class="form-control" name="LName" aria-describedby="emailHelp" placeholder="Enter Last name">
                    </div>
                    <div class="form-group">
                        <label for="DateReg">Date Register</label>
                        <input type="date" class="form-control" name="DateRegister" aria-describedby="emailHelp" placeholder="Enter date">
                    </div>
                    <div class="form-group">
                        <label for="Address">Address</label>
                        <input type="text" class="form-control" name="Address" aria-describedby="emailHelp" placeholder="Enter Address">
                    </div>
                    <div class="form-group">
                        <label for="Phone">Phone</label>
                         <input class="form-control" type="tel" value="09061002834" name="Phone" placeholder="Enter Contact number">
                    </div>
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="text" class="form-control" name="Email" aria-describedby="emailHelp" placeholder="Enter Email">
                    </div>
                    <button type="submit" class="btn btn-primary" value="save">Submit</button>
                </form>
            </div>
            </div>
        </div>
        </div>

                     <table class="table">
            <thead class="thead-dark">
                <tr>
                <th i="col">#</th>
                <th i="col">First Name</th>
                <th i="col">Last Name</th>
                <th i="col">Date of Register</th>
                <th i="col">Address</th>
                <th i="col">Phone</th>
                <th i="col">Email</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($result as $row) {?>
                <tr>
                <th i="row"><?php echo $row->id; ?></th>
                <td><?php echo $row->FirstName; ?></td>
                <td><?php echo $row->LastName; ?></td>
                <td><?php echo $row->DateRegister; ?></td>
                <td><?php echo $row->Address; ?></td>
                <td><?php echo $row->Phone; ?></td>
                <td><?php echo $row->Email; ?></td>

                <td> <a href="<?php echo site_url('Welcome/edit');?>/<?php echo $row->id;?>">Edit</a>  | 
                   <a href="<?php echo site_url('Welcome/delete');?>/<?php echo $row->id;?>">Delete</a> </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>