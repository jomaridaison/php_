<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    function createData() {
        $data = array (
            'FirstName' => $this->input->post('FName'),
            'LastName' => $this->input->post('LName'),
            'DateRegister' => $this->input->post('DateRegister'),
            'Address' => $this->input->post('Address'),
            'Phone' => $this->input->post('Phone'),
            'Email' => $this->input->post('Email')
        );
        $this->db->insert('exam_info', $data);
    }

    function getAllData() {
        $query = $this->db->query('SELECT * FROM exam_info');
        return $query->result();
    }

    function getData($id) {
        $query = $this->db->query('SELECT * FROM exam_info WHERE `id` =' .$id);
        return $query->row();
    }

    function updateData($id) {
        $data = array (
            'FirstName' => $this->input->post('FName'),
            'LastName' => $this->input->post('LName'),
            'DateRegister' => $this->input->post('DateRegister'),
            'Address' => $this->input->post('Address'),
            'Phone' => $this->input->post('Phone'),
            'Email' => $this->input->post('Email')

        );
        $this->db->where('id', $id);
        $this->db->update('exam_info', $data);
    }

    function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('exam_info');
    }
}